# Zebra Striped List

This problem demonstrates the implementation of a zebra-striped list implemented
with each of CSS, JS and PHP.

## Requirements

* Three separate pages
* Each has a list with at least 5 elements
* Each list has rows that alternate colours
