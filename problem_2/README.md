# Lead Generation Page

This problem contains a sample lead generation page for a fictitious product.
The page contains a form which allows a user to enter their name, select their
preferred contact method (email or phone) and then provide their contact information
(either email or phone). There is no backend.

## Requirements

### General

* A single page
* External CSS and JS assets
* Page should load and function as committed to the repo
	* If any build steps are required to modify, detail all software and build
  instructions
* Page should include:
	* Header / Navigation
	* Lead Form
	* Footer
* Functional on all screen sizes (responsive/fluid/etc)

### Form

* Include `Name` field
* Create field for user to select their preferred contact method: email or phone
	* If email is selected, show a `Email` field
	* If phone is selected, show a `Phone` field
* Submit button
