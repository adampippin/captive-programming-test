# Captive Programming Test

This repository contains three sample problems to gauge your skill with web
development. Please fork this repository, complete whichever you are able to,
and ensure the eventual repository is shared back with Adam Pippin (BitBucket
username `adampippin`, adamp@captive.ca.)

Each problem contains a `README.md` file outlining in general terms what the
problem entails and the requirements of the finished project.

## Problem 1

Develop a zebra-striped list. Stripe one with only CSS, one with JavaScript,
and one with PHP.

## Problem 2

Cut up a basic webpage containing a header, footer, and form with a few
simple fields. Implement some functionality to hide and show form fields
as appropriate.

## Problem 3

Take the webpage you developed in Problem #2 and implement it on top of the
'legacy' project contained within. Implement a new form field.

# Questions

Please send any questions or requests for clarification to adamp@captive.ca.

Good luck and thank you for taking the time to help us evaluate the candidates for
this position!