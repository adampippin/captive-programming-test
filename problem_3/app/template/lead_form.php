<?php static::template(APP_PATH.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.'header.php', get_defined_vars()) ?>

		<?php if ($saved) { ?>
			<p class="success"><?=$message?></p>
		<?php } else { ?>
			<?php if (!empty($message)) { ?>
			<p class="error"><?=$message?></p>
			<?php } ?>

			<form method="post">
				<label for="Name">
					Name
					<input type="text" name="Name" id="Name">
				</label>
				<label for="Email">
					Email
					<input type="text" name="Email" id="Email">
				</label>

				<button type="submit">Submit</button>
			</form>
		<?php } ?>

<?php static::template(APP_PATH.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.'footer.php', get_defined_vars()) ?>