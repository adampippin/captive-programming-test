<?php

namespace Captive\DumbVC;

class View
{
	protected static function model()
	{
		$class_name = get_called_class();
		$class_name = str_replace("View\\", "", $class_name);
		return new $class_name();
	}

	protected static function template($TEMPLATE_PATH, array $TEMPLATE_VARS = null)
	{
		if (isset($TEMPLATE_VARS))
			extract($TEMPLATE_VARS, EXTR_SKIP | EXTR_REFS);
		require($TEMPLATE_PATH);
	}

}