<?php

namespace Captive\DumbVC;

class Controller
{
	public static function model()
	{
		$class_name = get_called_class();
		$class_name = str_replace("Controller\\", "", $class_name);
		return new $class_name();
	}

	public static function view()
	{
		$class_name = get_called_class();
		$class_name = str_replace("Controller\\", "View\\", $class_name);
		return new $class_name();
	}

}