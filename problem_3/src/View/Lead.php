<?php

namespace View;

class Lead extends \Captive\DumbVC\View
{

	public static function display_form($message=null, $saved=false)
	{
		static::template(APP_PATH.'template'.DIRECTORY_SEPARATOR.'lead_form.php', [
			'message'=>$message,
			'saved'=>$saved
		]);
	}
	
}

?>
