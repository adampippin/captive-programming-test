<?php

class Lead
{
	use \Captive\DumbVC\Model;

	const LEADS_CSV_FILE = APP_PATH.'db'.DIRECTORY_SEPARATOR.'leads.csv';
	protected static $_data;

	public function __construct()
	{
		$this->readData();
	}

	public function __destruct()
	{
		$this->writeData();
	}

	public function append($name, $email=null)
	{
		static::$_data[] = [
			$name,
			$email
		];
	}

	protected function readData()
	{
		if (!isset(static::$_data))
		{
			if (file_exists(static::LEADS_CSV_FILE))
			{
				$data = [];
				$fp = fopen(static::LEADS_CSV_FILE, 'r');
				while ($line = fgetcsv($fp))
				{
					$data[] = $line;
				}
				fclose($fp);
			}
			else
			{
				$data = [['Name', 'Email']];
			}

			static::$_data = $data;
		}
	}

	protected function writeData()
	{
		if (isset(static::$_data) && is_array(static::$_data))
		{
			$fp = fopen(static::LEADS_CSV_FILE, 'w');
			foreach (static::$_data as $lead)
			{
				fputcsv($fp, $lead);
			}
			fclose($fp);
		}
	}


}

?>