<?php

namespace Controller;

class Lead extends \Captive\DumbVC\Controller
{

	public static function display_form()
	{
		static::view()->display_form();
	}

	public static function create_new()
	{
		$name = isset($_POST['Name']) && !empty($_POST['Name']) ? $_POST['Name'] : null;
		$email = isset($_POST['Email']) && !empty($_POST['Email']) ? $_POST['Email'] : null;

		if (!isset($name) || !isset($email))
		{
			return static::view()->display_form('Please fill in all the fields.');
		}

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
		{
			return static::view()->display_form('Please enter a valid email address.');
		}

		static::model()->append($name, $email);

		static::view()->display_form('Thank you! We will be in touch!', true);
	}

}

?>
