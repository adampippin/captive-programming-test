<?php

ini_set('display_errors', 'on');
error_reporting(-1);

class Application
{
	protected $router;

	public static function main()
	{
		$app = new Application();
		$app->initialize();
		$app->dispatch();
	}

	public function initialize()
	{
		define('BASE_PATH', __DIR__.DIRECTORY_SEPARATOR);
		define('APP_PATH', BASE_PATH.'app'.DIRECTORY_SEPARATOR);
		define('CONFIG_PATH', BASE_PATH.'config'.DIRECTORY_SEPARATOR);
		define('SRC_PATH', BASE_PATH.'src'.DIRECTORY_SEPARATOR);
		require(__DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php');
		$this->loadRoutes();
	}

	protected function loadRoutes()
	{
		$router = $this->router = new Xesau\Router(function ($method, $path, $statusCode, $exception) {
			http_response_code($statusCode);
			die('Error '.$statusCode);
		});

		require(CONFIG_PATH.'routes.php');
	}

	public function dispatch()
	{
		$this->router->dispatchGlobal();
	}

}

Application::main();