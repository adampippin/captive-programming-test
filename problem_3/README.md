# Lead Generation Project

This problem contains the (fake) legacy application the client is using for lead
generation. They really like your lead generation page and would like to have their
site updated to use it.

## Requirements

* Modify existing project to use the page template developed in Problem #2.
* Code does not currently support a phone field - add support for that.
* Implement any validation as appropriate.

If you want to show off, also consider:

* Implement any improvements you feel appropriate.
* Submit the form and fetch the response with AJAX.
